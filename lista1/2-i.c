
#include <stdio.h>

#define NCED 7

int cedulas[NCED] = {100,50,20,10,5,2,1};

int main() {

  int v;

  printf("Valor em reais: ");
  scanf("%d", &v);

  int ced, res;
  for (int i=0; i<7; i++ ){
    ced = cedulas[i];
    res = v / ced;
    v = v % ced;
    
    printf("%3d: %d\n", ced, res);
  }

  return 0;

}


