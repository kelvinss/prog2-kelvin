
#include <stdio.h>

int main(){
  int i = 12345;
  float x = 345.678;
  printf("%3d %5d %8d %f\n\n", i, i, i, x);
  printf("%3.2f %10f %13f %2.4f\n\n", x, x, x, x);
  printf("%3e %10f %13e\n\n", x, x, x);
}

