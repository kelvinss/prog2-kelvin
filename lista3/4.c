
#include <stdio.h>
#include <limits.h>

typedef unsigned int uint;

int main() {
  
  uint n;
  scanf("%d", &n);

  uint maior=0, menor=UINT_MAX;
  uint pares=0, impares=0, soma=0;

  //scanf(" %d", &x);
  //maior = menor = x;

  uint x;
  for (uint i=0; i<n; i++){
  	scanf(" %d", &x);
  	if (x>maior) maior = x;
  	if (x<menor) menor = x;
  	if (x%2==0)
  		pares++;
  	else
  		impares++;
  	soma += x;
  }
  
  printf("%d %d %d %d %.6f\n", maior, menor, pares, impares, (float)soma/n );
  
  return 0;
}

