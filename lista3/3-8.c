
#include <stdio.h>
#include <math.h>

int main() {

  float lim;
  scanf("%f", &lim);

  float soma=0, nsoma=0;
  int i=0;
  while (nsoma < lim) {
    soma = nsoma;
    nsoma += exp(i++);
  }
  i--;

  printf("%d %.6f\n", i, soma);

  return 0;
}
