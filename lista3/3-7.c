
#include <stdio.h>
#include <math.h>

int main() {
  int n;
  scanf("%d", &n);

  if (n < 2) { // Menor que 2
    printf("Nao primo\n");
    return 0;   
  }
  else if (n%2==0) { // Divisível por 2
    if (n==2) {
      printf("Primo\n");
      return 0;
    }
    printf("Nao primo\n");
    return 0;
  }
  else    // De 2 em 2 até a raiz quadrada (ignorando pares)
    for (int i=3; i<=sqrt(n); i+=2)
      if (n%i==0) {
        printf("Nao primo\n");
        return 0;
      }

  printf("Primo\n");
  return 0;
}

