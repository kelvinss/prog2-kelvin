
#include <stdio.h>

// Primos contidos em um intervalo

int main() {
  
  int n, m;
  
  printf("Extremos do intervalo: ");
  scanf("%d %d", &n, &m);
  
  for (int i=n+1; i < m; i++) {
    int divs = 0;
    for (int j=1; j<=i; j++)
      if (i % j == 0) divs++;

    if (divs == 2)
      printf("%d ", i);
  }
  
  return 0;
}


