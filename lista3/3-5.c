
#include <stdio.h>

int main() {
  int x;
  scanf("%d", &x);

  int soma = 0; 
  int maior = x;
  int n = 1;

  while (x != 0) {
    if (x > maior)  maior = x;
    soma += x;
    printf( "%d %.6lf \n", maior, (double)soma/n );
    n++;
    scanf("%d", &x);
  }
  
  return 0;
}

