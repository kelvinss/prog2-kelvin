
#include <stdio.h>
#include <limits.h>

int main() {

  int n;
  scanf("%d", &n);

  int maior=0, menor=INT_MAX;
  int pares=0, impares=0, soma=0;

  //scanf(" %d", &x);
  //maior = menor = x;

  int x;
  for (int i=0; i<n; i++){
  	scanf("%d", &x);
  	if (x>maior) maior = x;
  	if (x<menor) menor = x;
  	if (x%2==0)
  		pares++;
  	else
  		impares++;
  	soma += x;
  }

  printf("%d %d %d %d %.6f\n", maior, menor, pares, impares, (float)soma/n );

  return 0;
}
