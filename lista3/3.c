
#include <stdio.h>

// MDC - Máximo Divisor Comum

int main() {
  
  int n,m;

  printf("Inserir números para MDC: ");
  scanf("%d %d", &n, &m);

  int mdc;
  for (int i=n; 0<i; i--)
  	if (n%i==0 && m%i==0){
  		mdc = i;
  		break;
  	}

  printf("%d\n", mdc);
  
  return 0;
}

