
#include <stdio.h>

#define TAM 100

int ehPerfeito(int n){
  int soma = 1;

  for (int i=2; i<=n/2; i++)
    if(!(n%i))
      soma += i;

  return ( n == soma );
}

int pedePositivo(char* msg){
  int n;
  do {
    printf("%s", msg);
    scanf("%d", &n);
  } while (n<0);
  return n;
}

int preenchePerfeitos(int* vet, int* tm, int n){
  int i=0;
  while (*tm<n) {
    if ( ehPerfeito(i) )
      vet[(*tm)++] = i;
    i++;
  }
}

void printaInts(int* vet, int size){
  for (int i=0; i<size; i++)
    printf("%d: %d\n", i, vet[i] );
}

int encontraPosicao(int* vet, int tm, int x){
  int pos=0;
  while ( x >= vet[pos] && pos < tm )
    pos++;
  return pos;
}

int desloca1ApartirDe(int* vet, int* tm, int pos){
  for(int i = *tm-1; i>=pos; i--)
    vet[i+1] = vet[i];
  (*tm)++;
}

int insOrd(int* vet, int* tm, int x){
  int pos = encontraPosicao(vet, *tm, x);
  desloca1ApartirDe(vet, tm, pos);
  vet[pos] = x;
}

int main(){

  int n;
  n = pedePositivo("Entre com a quantidade de números perfeitos: ");

  int vetor[TAM],tm=0;

  preenchePerfeitos(vetor,&tm,n);

  printf("Números perfeitos encontrados:\n");
  printaInts(vetor, tm);

  int x;
  x = pedePositivo("Entre com o valor a ser inserido: ");

  insOrd(vetor, &tm, x);

  printaInts(vetor, tm);

  return 0;
}
