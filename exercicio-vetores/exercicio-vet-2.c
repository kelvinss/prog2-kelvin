
#include <stdio.h>

#define TAM 100

typedef struct Vet {
  int len;
  int items[TAM];
} vetor;

int ehPerfeito(int n){
  int soma = 1;

  for (int i=2; i<=n/2; i++)
    if(!(n%i))
      soma += i;

  return ( n == soma );
}

int pedePositivo(char* msg){
  int n;
  do {
    printf("%s", msg);
    scanf("%d", &n);
  } while (n<0);
  return n;
}

int preenchePerfeitos(vetor* vet, int n){
  int tm = vet->len;
  int i=0;
  while (tm<n) {
    if ( ehPerfeito(i) )
      vet->items[(tm)++] = i;
    i++;
  }
  (vet->len) = tm;
}

void printaInts(vetor* vet){
  int size = vet->len;
  for (int i=0; i<size; i++)
    printf("%d: %d\n", i, vet->items[i] );
}

int encontraPosicao(vetor* vet, int x){
  int tm = vet->len;
  int pos=0;
  while ( x >= vet->items[pos] && pos < tm )
    pos++;
  return pos;
}

int desloca1ApartirDe(vetor* vet, int pos){
  for(int i = vet->len-1; i>=pos; i--)
    vet->items[i+1] = vet->items[i];
  (vet->len)++;
}

int insOrd(vetor* vet, int x){
  int pos = encontraPosicao(vet, x);
  desloca1ApartirDe(vet, pos);
  vet->items[pos] = x;
}

int main(){

  int n;
  n = pedePositivo("Entre com a quantidade de números perfeitos: ");

  vetor vet;
  vet.len=0;

  preenchePerfeitos(&vet,n);

  printf("Números perfeitos encontrados:\n");
  printaInts(&vet);

  int x;
  x = pedePositivo("Entre com o valor a ser inserido: ");

  insOrd(&vet, x);

  printaInts(&vet);

  return 0;
}
