#include <stdio.h>

#define SIMNAO(x)  ((x) ? "Sim" : "Não")

typedef unsigned int  uint;
typedef enum { false, true } bool;



// Ex 3: Verifica se um inteiro é palíndromo

int inverterNumero (int n){
  int res=0;
  char d;
  while (n != 0){
    d = n % 10;
    n /= 10;
    res *= 10;
    res += d;
  }
  return res;
}

int ex3 (){
  printf("Insira o número: ");

  int n;
  scanf("%d", &n);

  if ( n == inverterNumero(n) )
    printf("O número é palíndromo\n");
  else
    printf("O número não é palíndromo\n");

  return 0;
}




// Ex 4: Verifica sufixo em inteiros

// bool ehPrefixo (uint n, uint pref) {
//   do {
//     if (n==pref)  return 1;
//     n /= 10;  // Retira o último digito
//   } while (n>=pref);
//
//   return 0;
// }

bool ehSufixo (int n, int suf) {
  if (suf == 0) return true;

  int d1 = n%10 , d2 = suf%10;
  n /= 10;
  suf /= 10;

  return (d1 == d2) && ehSufixo(n,suf);
}

int ex4 () {


  int n,suf;
  do {
    printf("Digite o número e o sufixo: ");
    scanf("%d%d", &n, &suf);
  } while (n < 0 || suf < 0);

  printf( "É sufixo: %s \n", SIMNAO( ehSufixo(n,suf) ) ) ;

  return 0;
}


// Ex 5: fatorial iterativo

uint fatorial (uint n){
  uint fat = 1;
  for ( ; n > 1; n--)
    fat *= n;
  return fat;
}

int ex5(){
  int n;
  do {
    printf("Calcular fatorial de: ");
    scanf("%d", &n);
  } while (n<0);

  printf("%d\n", fatorial(n) );
  return 0;
}


// Ex 6: fatorial recursivo

uint fat(uint n){
  if (n<=0)  return 1;
  return n * fat(n-1);
}

int ex6(){
  int n;
  do {
    printf("Calcular fatorial de: ");
    scanf("%d", &n);
  } while (n<0);

  printf("%d\n", fat(n) );
  return 0;
}


// Ex 7: MDC iterativo

#define divisivel(x,d)  (!(x%d))

int menor(int a, int b){
  if (a<=b)  return b;
  return a;
}

int mdc (int a, int b){
  int mdc = 1;

  while (divisivel(a,2) && divisivel(b,2)){
    mdc *= 2;
    a /= 2;
    b /= 2;
  }

  int max = menor(a/2, b/2);
  if (divisivel(max,2)) max--;

  for (int i=max; i>=3; i-=2){
    while (divisivel(a,i) && divisivel(b,i)){
      mdc *= i;
      a /= 2;
      b /= 2;
    }
  }

  return mdc;
}

int ex7(){
  int n1,n2;
  do {
    printf("Fazer MDC de: ");
    scanf("%d%d", &n1, &n2);
  } while (n1<0 || n2<0);

  printf("%d\n", mdc(n1,n2) );

  return 0;
}


// Ex 8: MDC recursivo

int mdc_rec (int a, int b){
  int resto = (a%b);
  if ( !resto ){
    return b;
  } else {
    return mdc(b, resto);
  }
}

int ex8(){
  int n1,n2;
  do {
    printf("Fazer MDC de: ");
    scanf("%d%d", &n1, &n2);
  } while (n1<0 || n2<0);

  printf("%d\n", mdc_rec(n1,n2) );

  return 0;
}


// Ex9: exemplo de função recursiva: fibonacci

// int _fibbo(int a, int b){
//   if (a<=2) return 1;
//   return _fibbo()
// }

int fibbo(int n){
  if (n<=2) return 1;
  return fibbo(n-1) + fibbo(n-2);
}

int ex9(){
  int n;
  do {
    printf("Número de fibonacci da posição: ");
    scanf("%d", &n);
  } while (n<0);

  printf("%d\n", fibbo(n) );

  return 0;
}



// ==== //

// Executa a questão "EX"

#define EX 9

#define QUESTAO(n)  case n: return ex ## n(); //break;

int main(){

  switch (EX) {
    QUESTAO(3)
    QUESTAO(4)
    QUESTAO(5)
    QUESTAO(6)
    QUESTAO(7)
    QUESTAO(8)
    QUESTAO(9)
  }

  return 0;
}
