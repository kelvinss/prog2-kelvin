
#include <stdio.h>

/* Soma os termos primos da sequência de Fibonacci até uma dado limite */

int main () {
  int lim;
  printf("Limite da soma: ");
  scanf("%d", &lim);

  int f1=2, f2=3;   // Valores iniciais da sequência
  int soma=0, novasoma=0;

  while (1) {

    // Verifica se `f1` é primo
    int primo = 1;
    if (f1 > 2) {
      if (f1%2 == 0)
        primo = 0;
      else
        for (int i=3 ; i<f1 ; i+=2) {
          if (f1%i == 0) {
            primo = 0;
            break;
          }
        }
    } else if (f1 == 1) {
      primo = 0;
    }

    // Se o termo for primo
    if (primo) {
        // Adiciona o termo à soma
        novasoma += f1;

        // Se a soma ainda estiver dentro do limite
        if (novasoma <= lim) {
          printf("%d ", f1);    // Imprime o termo
          soma = novasoma;      // Armazena a nova soma
        } else {
          break;                // Senão termina o loop
        }
    }

    // Calcula o próximo termo da sequência de Fibonacci
    int prox = f1+f2;
    f1 = f2;
    f2 = prox;
  }
  printf("\n");

  printf("Soma = %d\n", soma);

  return 0;
}
