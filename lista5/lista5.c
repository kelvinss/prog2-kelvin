#include <stdio.h>

#define EX 7

#define TAM 100

typedef unsigned int uint;


// Ex 1: Menor elemento do vetor

int ex1(){
  float vetor[TAM];

  uint t;
  printf("Defina o tamanho do vetor: ");
  scanf("%d", &t);

  printf("Entre com os elementos:\n");
  for (uint i=0; i<t; i++){
    printf("%d: ", i);
    float entrada;
    scanf("%f", &entrada);
    vetor[i] = entrada;
  }

  float menor = vetor[0];

  for (uint i=1; i<t; i++){
    float e = vetor[i];
    if (e < menor)
      menor = e;
  }

  printf("Menor elemento: %.2f\n", menor);

  return 0;
}



// Ex 2: somar vetores

int ex2(){

  float vetor[TAM], vetor2[TAM];

  uint t;
  printf("Defina o tamanho dos vetores: ");
  scanf("%u", &t);

  printf("Entre com os elementos do vertor 1:\n");
  for (uint i=0; i<t; i++){
    printf("%u: ", i);
    float entrada;
    scanf("%f", &entrada);
    vetor[i] = entrada;
  }

  printf("Entre com os elementos do vertor 2:\n");
  for (uint i=0; i<t; i++){
    printf("%u: ", i);
    float entrada;
    scanf("%f", &entrada);
    vetor2[i] = entrada;
  }

  for (uint i=0; i<t; i++){
    //float e = vetor[i];
    vetor[i] = vetor[i] + vetor2[i];
  }

  printf("\nResultado:\n");
  for (uint i=0; i<t; i++){
    printf("%d: ", i);
    printf("%.2f\n", vetor[i]);
  }

  return 0;
}


// Ex 3: ver se o vetor permanece igual quando revertido

char obtemValor (){
  char c;
  scanf(" %c", &c);
  return c;
}

int ex3(){

  typedef char tipo;

  tipo vetor[TAM];
  uint t;

  printf("Entre com o tamanho do vetor: ");
  scanf("%u", &t);

  printf("Entre com os elementos do vetor: ");
  for (uint i=0; i<t; i++){
    vetor[i] = obtemValor();
  }
  
  for (uint i=0; i<t; i++){
      printf("%d: %d %c\n", i, vetor[i], vetor[i]);
  }

  printf("\n");

    int diferente = 0;
  for (uint i=0; i<t/2; i++){
    tipo e1 = vetor[i];
    tipo e2 = vetor[t-1-i];
    
    printf("%c : %c\n", e1, e2);
    
    if (e1 != e2){
        diferente = 1;
        break;
    }

    
  }
  
  printf("O vetor");
  if (diferente)
      printf(" NÃO");
  printf(" permanece igual quando invertido.\n");

  return 0;
}



// Ex 5: determina os índices de um elemento 'x' em um vetor

int ex5 (){

  int vetor[TAM], tam, x;

  printf("Entre com o elemento a ser procurado: ");
  scanf("%d", &x);

  do {
    printf("Entre com o tamanho do vetor: ");
    scanf("%d", &tam);
  } while (tam<0);

  printf("Entre com os elementos do vetor: \n");
  for (int e,i=0; i<tam; i++){
    printf("%d: ", i);
    scanf("%d", &e);
    vetor[i] = e;
  }

  int indices[TAM], inds=0;
  for (int e,i=0; i<tam; i++){
    e = vetor[i];
    if ( e == x ){
      indices[inds++] = i;
    }
  }

  if (inds > 0){
    printf("Índices das ocorrências de '%d': \n", x);
    for (int i=0; i<inds; i++)
      printf("%d\n", indices[i]);
  } else {
    printf("'%d' não está presente do vetor de tamanho:\ntam = %d\n", x, tam);
  }

  return 0;
}


// Ex 6: remover ocorrencias de um elemento do vetor

int ex6(){

  int vetor[TAM], tam, x;

  printf("Entre com o valor dos elementos a serem retirados: ");
  scanf("%d", &x);

  do {
    printf("Entre com o tamanho do vetor: ");
    scanf("%d", &tam);
  } while (tam<0);

  printf("Entre com os elementos do vetor: \n");
  for (int e,i=0; i<tam; i++){
    printf("%d: ", i);
    scanf("%d", &e);
    vetor[i] = e;
  }

  for (int i=0; i<tam; i++){
    if( x == vetor[i] ){
      tam--;
      for (int j=i; j<tam; j++)
        vetor[j] = vetor[j+1]; 
    }
  }

  printf("Resultado:\n");
  for (int i=0; i<tam; i++){
    printf("%d: ", i);
    printf("%d\n", vetor[i]);
  }

  return 0;
}



// Ex 7: verificar característica da interseção de um vetor menor B com um vetor A
// (todos items contidos, parcialmente contido, nenhum item contido)

int ex7(){
  
  int n1,n2,vetorA[TAM], vetorB[TAM];

  do {
    printf("Entre com o tamanho do primeiro vetor: ");
    scanf("%d", &n1);
  } while (n1<0);

  printf("Entre com os elementos do primeiro vetor: \n");
  for (int e,i=0; i<n1; i++){
    printf("%d: ", i);
    scanf("%d", &e);
    vetorA[i] = e;
  }

  do {
    printf("Entre com o tamanho do segundo vetor: ");
    scanf("%d", &n2);
  } while (n2<0 || n2 > n1);

  printf("Entre com os elementos do segundo vetor: \n");
  for (int e,i=0; i<n2; i++){
    printf("%d: ", i);
    scanf("%d", &e);
    vetorB[i] = e;
  }

  int todos=1;
  int algum=0;

  for(int i=0; i<n2; i++){

    int eB = vetorB[i];
    int esta = 0;

    for (int j=0; j<n1; j++){
      int eA = vetorA[j];
      if (eB == eA){
        esta = 1;
        break;
      }
    }

    if (esta){
        algum = 1;
        if (!todos)
          break;
    } else {
      todos = 0;
      if (algum)
        break;
    }

  }

  if (algum)
    if (todos)
      printf("Todos contidos");
    else
      printf("Parcialmente contido");
  else
    printf("Nenhum contido");

  printf("\n");

  return 0;

}



// Ex 9: inserir elemento em vetor

int ex9(){
  int vetor[TAM], tam, x, pos;

  do {
    printf("Entre com o tamanho do vetor: ");
    scanf("%d", &tam);
  } while (tam<0 || tam >= 99);

  printf("Entre com os elementos do vetor: \n");
  for (int e,i=0; i<tam; i++){
    printf("%d: ", i);
    scanf("%d", &e);
    vetor[i] = e;
  }

  printf("Entre com elemento a ser adicionado: ");
  scanf("%d", &x);

  do {
    printf("Entre com a posição: ");
    scanf("%d", &pos);
  } while (pos<0 || pos >= tam);

  for (int i=tam-1; i>=pos; i--){
    vetor[i+1] = vetor[i];
  }
  vetor[pos] = x;
  tam++;

  printf("Resultado: \n");
  for (int i=0; i<tam; i++){
    printf("%d: %d\n", i, vetor[i]);
  }

}



// ==== //


// Executa a questão "EX"

#define QUESTAO(n)  case n: return ex##n(); //break;

int main(){

  printf("EXERCÍCIO %d\n\n", EX);

  switch (EX) {
    QUESTAO(1)
    QUESTAO(2)
    QUESTAO(3)
    QUESTAO(5)
    QUESTAO(6)
    QUESTAO(7)
    QUESTAO(9)
  }

  return 0;
}

 
